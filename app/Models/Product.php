<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
// use Laravel\Sanctum\HasApiTokens;

class Product extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $table = 'store_products';

    protected $fillable = [
        'user_id',
        'store_id',
        'category_id',
        'brand_id',
        'code',
        'name',
        'purchase_price',
        'sale_price',
        'description',
        'image',
        'status',
    ];


}
