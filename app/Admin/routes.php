<?php

use Illuminate\Routing\Router;

Admin::routes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
    'as'            => config('admin.route.prefix') . '.',
], function (Router $router) {

    $router->get('/', 'HomeController@index')->name('home');
    $router->resource('merchants/users', UserController::class);
    $router->resource('merchants/stores', StoreController::class);
    $router->resource('merchants/brands', BrandController::class);
    $router->resource('merchants/categories', CategoryController::class);
    $router->resource('merchants/products', ProductController::class);
    $router->get('showdata', 'UserController@showdata');
});
