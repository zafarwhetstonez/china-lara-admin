<?php

namespace App\Admin\Controllers;

use App\Models\Brand;
use App\Models\Category;
use App\Models\Product;
use App\Models\Store;
use App\Models\User;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class ProductController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Product';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Product());
        $user = new User();

        $grid->column('id', __('Id'));
        $grid->column('user_id')->display(function ($roles) {
            return User::find($roles)->name;
        });
        $grid->column('store_id')->display(function ($roles) {
            return Store::find($roles)->store_name;
        });
        $grid->column('category_id')->display(function ($roles) {
            return Category::find($roles)->name;
        });
        $grid->column('brand_id')->display(function ($roles) {
            return Brand::find($roles)->name;
        });
        // $grid->column('store_id', __('Store id'));
        // $grid->column('category_id', __('Category id'));
        // $grid->column('brand_id', __('Brand id'));
        $grid->column('code', __('Code'));
        $grid->column('name', __('Name'));
        $grid->column('purchase_price', __('Purchase price'));
        $grid->column('sale_price', __('Sale price'));
        $grid->column('description', __('Description'));
        $grid->column('image', __('Image'));
        $grid->column('status', __('Status'));
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Product::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('user_id', __('User id'));
        $show->field('store_id', __('Store id'));
        $show->field('category_id', __('Category id'));
        $show->field('brand_id', __('Brand id'));
        $show->field('code', __('Code'));
        $show->field('name', __('Name'));
        $show->field('purchase_price', __('Purchase price'));
        $show->field('sale_price', __('Sale price'));
        $show->field('description', __('Description'));
        $show->field('image', __('Image'));
        $show->field('status', __('Status'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Product());

        $userModel = new User();
        $storeModel = new Store();
        $categoryModel = new Category();
        $barndModel = new Brand();

        $form->select('user_id')->options($userModel::all()->pluck('name', 'id'));
        $form->select('store_id')->options($storeModel::all()->pluck('store_name', 'id'));
        $form->select('category_id')->options($categoryModel::all()->pluck('name', 'id'));
        $form->select('brand_id')->options($barndModel::all()->pluck('name', 'id'));
        $form->text('code', __('Code'));
        $form->text('name', __('Name'));
        $form->number('purchase_price', __('Purchase price'));
        $form->number('sale_price', __('Sale price'));
        $form->textarea('description', __('Description'));
        $form->image('image', __('Image'));
        $form->number('status', __('Status'));

        return $form;
    }
}
