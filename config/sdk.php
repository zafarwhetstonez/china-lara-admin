<?php


return [
  'app_key'   => env('APP_ID'),
  'app_secret'  => env('APP_secret'),
  'sign_secret' => env('signSecret'),
  'state'    => time(),
  'redirect_uri' => 'http://kuaishou.growingsale.com',
  'debug'    => false,
  'access_token' => 'access_token',
  'log'=>[
    'name'=>'kuaishou',
    'file'=> __DIR__.'/kuaishou.log',
    'level'=>'debug',
    'permission'=> 0777
  ]
];
